// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入文件控制器中的处理函数
const {getFileList, uploadFile} = require('../controllers/file');

// 当收到 GET 请求到 /get_file_list 路径时，调用 getFileList 函数获取文件列表。
// 当收到 POST 请求到 /upload_file 路径时，调用 uploadFile 函数处理上传文件的请求。

// 定义了两个路由规则，分别对应获取文件列表和上传文件的请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_file_list', getFileList);
router.post('/upload_file', uploadFile);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
