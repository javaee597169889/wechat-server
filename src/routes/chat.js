// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入聊天控制器中的处理函数
const {getChatList} = require('../controllers/chat');

// 当收到 GET 请求到 /get_chat_list 路径时，调用 getChatList 函数获取聊天列表。

// 定义了一个路由规则，对应获取聊天列表的请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_chat_list', getChatList);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
