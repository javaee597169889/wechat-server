// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入用户控制器中的处理函数
const {signin, login, getLogin, logout, getUserInfo} = require('../controllers/user');

// 当收到 POST 请求到 /signin 路径时，调用 signin 函数处理用户的注册请求。
// 当收到 POST 请求到 /login 路径时，调用 login 函数处理用户的登录请求。
// 当收到 GET 请求到 /getlogin 路径时，调用 getLogin 函数获取登录页面。
// 当收到 GET 请求到 /logout 路径时，调用 logout 函数处理用户的注销请求。
// 当收到 GET 请求到 /get_user_info 路径时，调用 getUserInfo 函数获取用户信息。

// 定义了多个路由规则，分别对应不同的用户请求
// 将定义好的路由规则绑定到路由器实例上
router.post('/signin', signin);
router.post('/login', login);
router.get('/getlogin', getLogin);
router.get('/logout', logout);
router.get('/get_user_info', getUserInfo);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
