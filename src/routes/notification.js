// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入通知控制器中的处理函数
const {getNotificationList} = require('../controllers/notification');

// 当收到 GET 请求到 /get_noti_list 路径时，调用 getNotificationList 函数获取通知列表。

// 定义了一个路由规则，对应获取通知列表的请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_noti_list', getNotificationList);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
