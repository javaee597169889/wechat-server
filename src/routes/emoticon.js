// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入表情符号控制器中的处理函数
const {getEmoticonList, addEmoticon} = require('../controllers/emoticon');

// 当收到 GET 请求到 /get_emoticon_list 路径时，调用 getEmoticonList 函数获取表情符号列表。
// 当收到 GET 请求到 /add_emoticon 路径时，调用 addEmoticon 函数处理添加表情符号的请求。

// 定义了两个路由规则，分别对应获取表情符号列表和添加表情符号的请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_emoticon_list', getEmoticonList);
router.get('/add_emoticon', addEmoticon);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
