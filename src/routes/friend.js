// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入好友控制器中的处理函数
const { addFriend, getFriendList, deleteFriend } = require('../controllers/friend');

// 当收到 GET 请求到 /add_friend 路径时，调用 addFriend 函数处理添加好友的请求。
// 当收到 GET 请求到 /get_friend_list 路径时，调用 getFriendList 函数获取好友列表。
// 当收到 GET 请求到 /delete_friend 路径时，调用 deleteFriend 函数处理删除好友的请求。

// 定义了三个路由规则，分别对应不同的好友请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/add_friend', addFriend);
router.get('/get_friend_list', getFriendList);
router.get('/delete_friend', deleteFriend);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
