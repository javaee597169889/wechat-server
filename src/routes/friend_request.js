// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入好友请求控制器中的处理函数
const {addRequest, getRequestList} = require('../controllers/friend_request');

// 当收到 GET 请求到 /get_friend_request_list 路径时，调用 getRequestList 函数获取好友请求列表。
// 当收到 GET 请求到 /add_friend_request 路径时，调用 addRequest 函数处理添加好友请求的请求。

// 定义了两个路由规则，分别对应获取好友请求列表和添加好友请求的请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_friend_request_list', getRequestList);
router.get('/add_friend_request', addRequest);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
