// 引入 Express 框架
const express = require('express');
// 创建一个新的路由器实例
const router = express.Router();
// 引入会话控制器中的处理函数
const { getSessionList, resetUnread } = require('../controllers/session');

// 当收到 GET 请求到 /get_session_list 路径时，调用 getSessionList 函数获取会话列表。
// 当收到 GET 请求到 /reset_unread 路径时，调用 resetUnread 函数重置未读消息数。

// 定义了两个路由规则，分别对应不同的会话请求
// 将定义好的路由规则绑定到路由器实例上
router.get('/get_session_list', getSessionList);
router.get('/reset_unread', resetUnread);

// 通过 module.exports 将路由器实例导出，以便在其他文件中使用
module.exports = router;
