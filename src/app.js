// 引入所需的模块和中间件
const express = require('express'); // 引入Express框架
const multer = require('multer'); // 引入multer用于处理文件上传
const http = require('http'); // 引入http模块用于创建HTTP服务器
const path = require('path'); // 引入path模块用于处理文件路径
const Session = require('express-session'); // 引入express-session用于处理会话管理
const middleware = require('./middleware/middleware');  // 引入自定义的中间件
const sharedsession = require('express-socket.io-session');

const userRouter = require('./routes/user');
const friendRouter = require('./routes/friend');
const sessionRouter = require('./routes/session');
const chatRouter = require('./routes/chat');
const friendRequestRouter = require('./routes/friend_request');
const notificationRouter = require('./routes/notification');
const fileRouter = require('./routes/file');
const emoticonRouter = require('./routes/emoticon');

// 创建Express应用程序实例
const app = express();

// 配置multer中间件来处理文件上传
app.use(multer({
    // dest: '../uploadFile', // 设置文件上传的目标目录（如果需要保存到服务器）
    limits: {fileSize: 2 * 1024 * 1024} // 设置文件大小限制为2MB
}).array('file')) // 使用multer处理文件上传，上传字段名为'file'
app.use(express.static('../uploadFile')); // 设置静态文件目录，用于访问上传的文件

// 配置Express中间件来解析JSON和表单数据
app.use(express.json()); // 解析JSON请求体
app.use(express.urlencoded({extended: false})); // 解析表单数据

// 配置express-session中间件来处理会话管理
const session = Session({
    // 可自定义
    secret: "1234567890",
    resave: true,
    saveUninitialized: true,
});

// 将会话管理中间件添加到Express应用程序中
app.use(session);

// 使用自定义的身份验证中间件（authGuard）
app.use(middleware.authGuard);

// 配置路由器来处理不同的API请求
app.use('/api/users', userRouter); // 处理用户相关API
app.use('/api/friends', friendRouter); // 处理好友相关API
app.use('/api/sessions', sessionRouter); // 处理会话相关API
app.use('/api/chats', chatRouter); // 处理聊天相关API
app.use('/api/friend_requests', friendRequestRouter); // 处理好友请求相关API
app.use('/api/notis', notificationRouter); // 处理通知相关API
app.use('/api/files', fileRouter); // 处理文件相关API
app.use('/api/emoticons', emoticonRouter); // 处理表情符号相关API

// 创建HTTP服务器，并使用Express应用程序实例作为处理程序
const server = http.createServer(app);

// 引入WebSocket模块并将Express的会话管理中间件与WebSocket共享
const socket = require("./websocket/sockio.js");
process.io = socket.getSocket(server); // 使用http协议建立socket
process.io.use(sharedsession(session)); // 使用Express的会话管理中间件与WebSocket共享

// 启动HTTP服务器监听在3010端口上
server.listen(3010);

// Express应用程序监听在3000端口上
app.listen(3000, () => {
    console.log('localhost:3000');
});
