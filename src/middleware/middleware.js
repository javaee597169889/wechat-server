// 引入用户数据访问对象 dao_user
const dao_user = require('../dao/user')

// 定义了一个对象 non_auth_req，用于存储不需要进行权限校验的请求路径
// 这些路径表示用户登录、注册和上传文件等操作，通常是不需要进行登录状态验证的
const non_auth_req = {
    '/api/users/login': true,
    '/api/users/signin': true,
    '/api/files/upload_file': true,
}

// 定义了一个名为 authGuard 的中间件函数，接受请求(req)、响应(res)和下一个中间件函数(next)作为参数
const authGuard = async (req, res, next) => {
    // 检查当前请求的路径是否在 non_auth_req 对象中，如果是，则直接调用下一个中间件
    if (non_auth_req[req.url]) {
        next();
        return;
    }

    // 如果请求路径不在 non_auth_req 对象中，则获取请求中的用户ID（假设存储在会话中），然后通过用户数据访问对象 dao_user 获取对应的用户信息
    const userId = req.session["userId"]
    const user = userId && await dao_user.getUserById(userId)

    // 如果无法获取到用户信息（即用户未登录），则返回 401 未授权的状态码，并发送包含错误消息的 JSON 响应
    if (!user) {
        res.end(JSON.stringify({
            code: 401,
            msg: '未登录',
        }))
        return
    }

    // 如果能够获取到用户信息，则调用下一个中间件函数
    next()
}

// 通过 module.exports 将 authGuard 中间件导出，以便在其他文件中使用
module.exports = {
    authGuard,
}
