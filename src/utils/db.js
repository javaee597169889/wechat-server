// 引入Sequelize模块
const DB = require('sequelize')

// 'socket_chat' 是数据库的名称。
// 'root' 是MySQL的用户名。
// 'admin' 是MySQL的密码。
// 'localhost' 是MySQL数据库的主机名。
// 'mysql' 是指定使用MySQL作为数据库的方言（dialect）。
// { charset: 'utf8mb4' } 用于指定数据库的字符集为UTF-8，这样可以支持存储更广泛的字符

// 创建Sequelize实例并连接到MySQL数据库
const sequelize = new DB.Sequelize('socket_chat', 'root', 'djdk597169889', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        charset: 'utf8mb4'
    }
});

// 导出sequelize对象，以便在其他文件中使用该数据库连接实例
module.exports = {
    sequelize
}
