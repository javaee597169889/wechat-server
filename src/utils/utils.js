// 内部的 S4 函数用于生成一个4位的随机十六进制数。
// (((1 + Math.random()) * 0x10000) | 0) 用于生成一个随机数，并将其转换为十六进制字符串。
// substring(1) 用于移除生成的随机数中的第一个字符，以确保生成的字符串始终为4位。
// 最后，将多个 S4 函数的结果拼接起来，形成一个符合UUID标准的唯一标识符。

// guid 函数用于生成UUID
const guid = function () {
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
    }

    return (S4() + S4() + '-' + S4() + '-' + S4() + '-' + S4() + '-' + S4() + S4() + S4())
}

// Math.random() 用于生成一个0到1之间的随机数。
// Math.pow(10, 6) 用于计算10的6次方，即生成一个百万级的数。
// Math.round() 用于将生成的随机数四舍五入为整数。
// toString() 用于将生成的随机数转换为字符串格式。

// createSessionId 函数用于生成会话ID
const createSessionId = function () {
    return Math.round(Math.random() * Math.pow(10, 6)).toString()
}

// 通过 module.exports 将这两个函数导出，以便在其他文件中使用
module.exports = {
    guid,
    createSessionId,
}
