// 引入了 Sequelize 库和数据库实例
const DB = require('sequelize')
const {sequelize} = require('../utils/db');

// id: 文件记录的唯一标识，类型为整数（INTEGER），是主键（primaryKey），并且是自增的（autoIncrement）。
// userId: 文件所属用户的 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// fileName: 文件名，类型为字符串（STRING），不能为空（allowNull: false）。
// src: 文件地址，类型为字符串（STRING），不能为空（allowNull: false）。
// size: 文件大小，类型为整数（INTEGER）。
// createdAt: 文件创建时间，类型为字符串（STRING）。
// type: 文件类型，类型为整数（INTEGER），默认值为 0，有注释说明不同的数字对应不同的文件类型，如图片或文件。

// 使用 sequelize.define 方法定义了一个名为 File 的数据模型，描述了 files 表的结构和约束
// 在第二个参数中设置了一些额外的选项
// user数据表
const File = sequelize.define('files', {
    id: {
        type: DB.INTEGER,
        primaryKey: true, //主键
        autoIncrement: true, //自增
        comment: "自增id" //注释:只在代码中有效
    },
    // 用户id
    userId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    fileName: {
        type: DB.STRING,
        allowNull: false,
    },
    src: {
        type: DB.STRING,
        allowNull: false,
    },
    size: {
        type: DB.INTEGER,
    },
    createdAt: {
        type: DB.STRING,
    },
    type: {
        type: DB.INTEGER,
        default: 0,
        comment: '文件类型: 1:图片 2:文件 ',
    }
}, {

    // freezeTableName: true: 表示使用自定义的表名，即不会将模型名 File 转换成复数形式并用作表名。
    // timestamps: false: 表示不需要 Sequelize 自动生成添加时间和更新时间的字段。

    //使用自定义表名
    freezeTableName: true,
    //默认的添加时间和更新时间
    timestamps: false,
});

// 调用 File.sync() 方法进行数据模型和数据库表的同步操作，确保数据库中存在对应的表格。sync() 方法会根据模型定义自动创建对应的表格，如果已经存在则不做任何变化
//同步:没有就新建,有就不变
File.sync();

// 通过 module.exports 导出 File 模型，以便在其他文件中使用
module.exports = File;
