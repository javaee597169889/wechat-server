// 引入了 Sequelize 库和数据库实例
const DB = require('sequelize')
const {sequelize} = require('../utils/db');

// id: 表情的唯一标识，类型为整数（INTEGER），是主键（primaryKey），并且是自增的（autoIncrement）。注释说明了它是自增 id。
// userId: 用户 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// src: 表情图片的路径，类型为字符串（STRING），不能为空（allowNull: false）。
// createdAt: 创建时间，类型为字符串（STRING）。

// 使用 sequelize.define 方法定义了一个名为 Emoticon 的数据模型，描述了 emoticons 表的结构和约束
// 在第二个参数中设置了一些额外的选项
// user数据表
const Emoticon = sequelize.define('emoticons', {
    id: {
        type: DB.INTEGER,
        primaryKey: true, //主键
        autoIncrement: true, //自增
        comment: "自增id" //注释:只在代码中有效
    },
    // 用户id
    userId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    src: {
        type: DB.STRING,
        allowNull: false,
    },
    createdAt: {
        type: DB.STRING,
    },
}, {

    // freezeTableName: true: 表示使用自定义的表名，即不会将模型名 Emoticon 转换成复数形式并用作表名。
    // timestamps: false: 表示不需要 Sequelize 自动生成添加时间和更新时间的字段。

    //使用自定义表名
    freezeTableName: true,
    //默认的添加时间和更新时间
    timestamps: false,
});

// 调用 Emoticon.sync() 方法进行数据模型和数据库表的同步操作，确保数据库中存在对应的表格。sync() 方法会根据模型定义自动创建对应的表格，如果已经存在则不做任何变化
//同步:没有就新建,有就不变
Emoticon.sync();

// 通过 module.exports 导出 Emoticon 模型，以便在其他文件中使用
module.exports = Emoticon;
