// 引入了 Sequelize 库和数据库实例
const DB = require('sequelize')
const {sequelize} = require('../utils/db');

// id: 会话记录的唯一标识，类型为整数（INTEGER），是主键（primaryKey），并且是自增的（autoIncrement）。注释说明了它是自增 id。
// sessionId: 会话的 ID，类型为字符串（STRING），不能为空（allowNull: false）。
// userId: 用户 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// receiverId: 接收者 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// lastChat: 最后一次聊天记录，类型为字符串（STRING）。
// type: 会话类型，类型为整数（INTEGER），默认值为 0，有注释说明不同的数字对应不同的会话类型，如一对一聊天、群聊或系统通知。
// read: 未读条数，类型为整数（INTEGER），默认值为 0，有注释说明这个字段的含义。
// updatedAt: 更新时间，类型为字符串（STRING）。注意这里应该是 comment 而不是 commit，这是一个小错误。

// 使用 sequelize.define 方法定义了一个名为 Session 的数据模型，描述了 sessions 表的结构和约束
// 在第二个参数中设置了一些额外的选项
// user数据表
const Session = sequelize.define('sessions', {
    id: {
        type: DB.INTEGER,
        primaryKey: true, //主键
        autoIncrement: true, //自增
        comment: "自增id" //注释:只在代码中有效
    },
    sessionId: {
        type: DB.STRING,
        allowNull: false, //不允许为null
    },
    // 用户id
    userId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 接收者id
    receiverId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 最后一次聊天记录
    lastChat: {
        type: DB.STRING,
    },
    type: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '0:一对一聊天  1:群聊  2:系统通知'
    },
    // 未读条数
    read: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '未读条数'
    },
    // 更新时间
    updatedAt: {
        type: DB.STRING,
        comment: '更新时间'
    }
}, {

    // freezeTableName: true: 表示使用自定义的表名，即不会将模型名 Session 转换成复数形式并用作表名。
    // timestamps: false: 表示不需要 Sequelize 自动生成添加时间和更新时间的字段。


//使用自定义表名
    freezeTableName: true,
    //默认的添加时间和更新时间
    timestamps: false,
});

// 调用 Session.sync() 方法进行数据模型和数据库表的同步操作，确保数据库中存在对应的表格。sync() 方法会根据模型定义自动创建对应的表格，如果已经存在则不做任何变化。
//同步:没有就新建,有就不变
Session.sync();

// 通过 module.exports 导出 Session 模型，以便在其他文件中使用。
module.exports = Session;
