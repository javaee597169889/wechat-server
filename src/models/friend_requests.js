// 引入了 Sequelize 库和数据库实例
const DB = require('sequelize')
const {sequelize} = require('../utils/db');

// id: 好友请求的唯一标识，类型为整数（INTEGER），是主键（primaryKey），并且是自增的（autoIncrement）。注释说明了它是自增 id。
// userId: 发起好友请求的用户 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// friendId: 被请求加为好友的用户 ID，类型为整数（INTEGER），不能为空（allowNull: false）。
// content: 请求的理由，类型为字符串（STRING），有一个注释指示了字段的含义。
// read: 是否已读，类型为整数（INTEGER），默认值为 0，有一个注释说明了可能的取值。在这个示例中，0 表示未读，1 表示已读。
// handle: 是否已处理，类型为整数（INTEGER），默认值为 0，有一个注释说明了可能的取值。在这个示例中，0 表示未处理，1 表示已处理。
// createdAt: 创建时间，类型为字符串（STRING）。

// 使用 sequelize.define 方法定义了一个名为 FriendRequest 的数据模型，描述了 friend_requests 表的结构和约束
// 在第二个参数中设置了一些额外的选项
// user数据表
const FriendRequest = sequelize.define('friend_requests', {
    id: {
        type: DB.INTEGER,
        primaryKey: true, //主键
        autoIncrement: true, //自增
        comment: "自增id" //注释:只在代码中有效
    },
    // 用户id
    userId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 好友id
    friendId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 请求理由
    content: {
        type: DB.STRING,
        comment: '请求理由'
    },
    // 是否已读
    read: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '0:未读  1:已读'
    },
    // 是否处理
    handle: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '0:未处理  1:已处理'
    },
    createdAt: {
        type: DB.STRING,
    },
}, {

    // freezeTableName: true: 表示使用自定义的表名，即不会将模型名 FriendRequest 转换成复数形式并用作表名。
    // timestamps: false: 表示不需要 Sequelize 自动生成添加时间和更新时间的字段。

    //使用自定义表名
    freezeTableName: true,
    //默认的添加时间和更新时间
    timestamps: false,
});

// 调用 FriendRequest.sync() 方法进行数据模型和数据库表的同步操作，确保数据库中存在对应的表格。sync() 方法会根据模型定义自动创建对应的表格，如果已经存在则不做任何变化
//同步:没有就新建,有就不变
FriendRequest.sync();

// 通过 module.exports 导出 FriendRequest 模型，以便在其他文件中使用
module.exports = FriendRequest;
