// 引入 Sequelize 库和数据库实例
const DB = require('sequelize')
const {sequelize} = require('../utils/db');

// id: 聊天记录的唯一标识，类型为整数（INTEGER），是主键（primaryKey），并且是自增的（autoIncrement）。
// sessionId: 会话 ID，类型为字符串（STRING），不能为空（allowNull: false）。
// senderId: 发送者的 ID，类型为整数（INTEGER），不能为空。
// receiverId: 接收者的 ID，类型为整数（INTEGER），不能为空。
// content: 聊天内容，类型为字符串（STRING），长度最大为 5000，有一个注释指明不同的类型对应的内容，如聊天信息、文件地址、通知内容等。
// type: 聊天内容的类型，类型为整数（INTEGER），默认值为 0，有注释说明不同的数字对应不同的类型，如文字、图片、文件、表情包、聊天窗口内通知等。
// sort: 聊天的分类，类型为整数（INTEGER），默认值为 0，有注释说明不同的数字对应私聊或群聊。
// updatedAt: 聊天记录的更新时间，类型为字符串（STRING），有一个注释说明是时间戳。
// others: 用于存储其他信息的字段，类型为字符串（STRING），长度最大为 1000，存储类型为 JSON，可以存储如文件的信息等。

// 使用 sequelize.define 方法定义了一个名为 Chat 的数据模型，它描述了 chats 表的结构和约束
// 在第二个参数中设置了一些额外的选项
// user数据表
const Chat = sequelize.define('chats', {
    id: {
        type: DB.INTEGER,
        primaryKey: true, //主键
        autoIncrement: true, //自增
        comment: "自增id" //注释:只在代码中有效
    },
    sessionId: {
        type: DB.STRING,
        allowNull: false,
    },
    // 发送者id
    senderId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 接收者id
    receiverId: {
        type: DB.INTEGER,
        allowNull: false, //不允许为null
    },
    // 内容
    content: {
        type: DB.STRING(5000),
        comment: 'type为0时:聊天信息  为1/2/3:文件地址   为4:通知内容'
    },
    // 类型
    type: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '0:文字   1: 图片   2:文件   3:表情包   4:聊天窗口内通知'
    },
    sort: {
        type: DB.INTEGER,
        defaultValue: 0,
        comment: '0:私聊   1: 群聊'
    },
    // 更新时间
    updatedAt: {
        type: DB.STRING,
        comment: '更新时间, 时间戳'
    },
    // 用于存储其它信息，存储类型为JSON，可以存储如文件的信息
    others: {
        type: DB.STRING(1000),
        comment: '用于存储其它信息，存储类型为JSON，可以存储如文件的信息',
    },
}, {

    // freezeTableName: true: 表示使用自定义的表名，即不会将模型名 Chat 转换成复数形式并用作表名。
    // timestamps: false: 表示不需要 Sequelize 自动生成添加时间和更新时间的字段。

    //使用自定义表名
    freezeTableName: true,
    //默认的添加时间和更新时间
    timestamps: false,
});

// 调用 Chat.sync() 方法进行数据模型和数据库表的同步操作，确保数据库中存在对应的表格。sync() 方法会根据模型定义自动创建对应的表格，如果已经存在则不做任何变化
//同步:没有就新建,有就不变
Chat.sync();

// 通过 module.exports 导出 Chat 模型，以便在其他文件中使用
module.exports = Chat;
