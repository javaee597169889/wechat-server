// dao_friend_request: 用于处理好友请求相关的数据库操作。
// dao_friend: 用于处理好友关系相关的数据库操作。
// dao_user: 用于处理用户相关的数据库操作。
// utils: 包含一些通用的工具函数。
// socket_user: 用于处理 WebSocket 相关的用户操作。

// 通过 require 关键字引入了几个模块
const dao_friend_request = require('../dao/friend_request')
const dao_friend = require('../dao/friend')
const dao_user = require('../dao/user')
const utils = require('./utils')
const socket_user = require('../websocket/user')

// 定义了一个名为 addRequest 的异步函数，用于保存好友请求。接受三个参数：req、res 和 next。
// 在函数内部，首先设置响应头 content-type 为 application/json;charset=utf-8，以确保返回的数据类型为 JSON 格式，并且指定了字符编码为 UTF-8。
// 获取请求中的查询参数 query，如果未提供 friendId 参数，则返回错误信息。
// 通过 req.session["userId"] 获取当前用户的用户 ID，并通过 dao_user.getUserById 获取当前用户的详细信息。
// 检查请求中的 friendId 是否为当前用户自己的 ID，如果是，则返回错误信息。
// 检查请求中的 friendId 对应的用户是否存在，如果不存在，则返回错误信息。
// 检查当前用户与指定用户是否已是好友，如果是，则返回错误信息。
// 判断好友请求是否已存在，如果存在，则更新请求内容和时间；如果不存在，则新增好友请求记录。
// 根据操作结果，返回相应的 JSON 数据，表示请求是否成功。
// 如果保存请求成功，且对方在线，则通过 WebSocket 发送好友请求通知。

// 保存好友请求
const addRequest = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    let query = req.query
    if (!query.friendId) {
        res.end(JSON.stringify({
            code: 400,
            msg: '缺少必要参数: friendId',
        }))
        return
    }
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    let user = await dao_user.getUserById(req.session["userId"])
    if (user.id == query.friendId) {
        res.end(JSON.stringify({
            code: 400,
            msg: 'friendId不能是自己哦',
        }))
        return
    }
    // 判断用户是否存在
    const friend = await dao_user.getUserById(query.friendId)
    if (!friend) {
        res.end(JSON.stringify({
            code: 400,
            msg: '用户不存在',
        }))
        return
    }
    // 判断是否已是好友
    const isFriend = await dao_friend.getIsFriend(user.id, query.friendId)
    if (isFriend) {
        res.end(JSON.stringify({
            code: 400,
            msg: '该用户已是你的好友',
        }))
        return
    }
    // 判断好友请求是否存在
    // 这里user和friend需要反过来
    let friendReq = await dao_friend_request.getRequest(query.friendId, user.id)
    let result = null;
    if (friendReq) {
        // res.end(JSON.stringify({
        //   code: 400,
        //   msg: '已经发送过好友请求了, 请耐心等待好友接受'
        // }))
        // return
        result = await dao_friend_request.updateRequest(friendReq.id, 0, 0, query.content, new Date().getTime())
    } else {
        // 这里user和friend需要反过来
        result = await dao_friend_request.addRequest(query.friendId, user.id, query.content)
    }
    res.end(JSON.stringify({
        code: result ? 200 : 400,
        msg: result ? '好友请求发送成功' : '请求失败'
    }))
    if (result) {
        // 判断对方是否在线，在线的话发送websocket通知
        let socketUser = socket_user.getUserById(query.friendId)
        if (!socketUser) return
        process.io.to(socketUser.socketId).emit('message', {
            msgType: 'friendRequest',
            userInfo: {
                id: user.id,
                userName: user.userName,
                avatar: user.avatar,
                email: user.email,
            },
            content: query.content
        })
    }
}

// 定义了一个名为 getRequestList 的异步函数，用于获取好友请求列表。同样接受三个参数：req、res 和 next。
// 在函数内部，设置响应头 content-type 为 application/json;charset=utf-8。
// 通过 req.session["userId"] 获取当前用户的用户 ID，并调用 dao_friend_request.getRequestList 获取该用户的好友请求列表。
// 返回包含请求列表的 JSON 数据。

// 获取好友列表
const getRequestList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const list = await dao_friend_request.getRequestList(req.session["userId"], req.query.lastTime)
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

// 通过 module.exports 导出了 addRequest 和 getRequestList 函数，使其可以被其他模块引用和使用
module.exports = {
    addRequest,
    getRequestList,
};
