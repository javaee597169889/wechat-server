// dao_file: 文件数据访问对象，用于处理文件相关的数据库操作。
// dao_emotion: 表情包数据访问对象，用于处理表情包相关的数据库操作。
// utils: 自定义的工具函数模块，包含了一些通用的工具方法。
// fs: Node.js 的文件系统模块，用于文件操作。
// path: Node.js 的路径处理模块，用于处理文件路径。
// getPixels: 一个用于获取图像像素数据的模块，可能是从图像文件中读取像素数据。

// 通过 require 引入了一些模块和文件
const dao_file = require('../dao/file')
const dao_emotion = require('../dao/emoticon')
const utils = require('./utils')
const fs = require('fs')
const path = require('path')
const getPixels = require("get-pixels")
// const dao_user = require('../dao/user')
// const socket_user = require('../websocket/user')

// getFileList 函数：用于获取文件列表。
// 首先设置了响应头的 content-type，然后获取请求中的查询参数，包括 lastTime。
// 接着调用 dao_file.getFileList 函数获取文件列表数据，并返回给客户端。

// 获取文件列表
const getFileList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const query = req.query;
    query.lastTime = query.lastTime || new Date().getTime()
    const list = await dao_file.getFileList(req.session['userId'], query.lastTime)
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

// uploadFile 函数：用于处理文件上传。
// 同样设置了响应头的 content-type。
// 首先判断上传的文件是否存在以及大小是否合适。
// 然后获取用户信息，判断文件类型和是否为表情包，以及用户是否登录。
// 接着判断文件夹是否存在，如果不存在则创建。生成文件路径，将文件写入到指定路径。
// 如果是表情包并且是图片文件，则异步获取图片尺寸，并根据尺寸进行处理，最后将表情包信息写入数据库。
// 最后返回上传结果给客户端。

// 上传文件 / 表情  （当传入emoticon:ture 参数时，为上传表情包）
const uploadFile = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // 判断文件是否存在文件以及文件大小
    let file = req.files[0]
    // console.log(file);
    if (!file || file.size > 2 * 1024 * 1024) {
        res.end(JSON.stringify({
            code: 400,
            msg: "file doesn't exist or too large"
        }))
        return
    }
    // const user = await utils.isLogin(req)
    const userId = req.session["userId"]
    // 获取文件类型
    let fileType = /image\/png|image\/jpeg|image\/gif/.test(file.mimetype) ? 'img' : 'file'
    if (req.body.emoticon && fileType === 'file') {
        res.end(JSON.stringify({
            code: 400,
            msg: "表情包只能为 jpg/png/gif 格式"
        }))
        return
    } else if (!userId && req.body.emoticon) {
        // 判断是否登录
        res.end(JSON.stringify({
            code: 401,
            msg: '添加表情需要登录哦',
        }))
        return
    }
    // 判断是否存在文件夹,没有就创建
    fs.existsSync("../uploadFile") || fs.mkdirSync("../uploadFile")
    let timestamps = new Date().getTime()
    let ext = path.extname(file.originalname)
    let dir = path.join('../uploadFile/', timestamps + ext)
    // let dir = '../uploadFile/' + timestamps + ext
    fs.writeFile(dir, file.buffer, async function (err) {
        if (err) console.log(err);
        // 文件写入成功
        let src = 'http://localhost:3000/' + timestamps + ext
        if (req.body.emoticon && fileType === 'img') {
            new Promise((resolve, reject) => {
                // 判断是否是图片
                getPixels('../uploadFile/' + timestamps + ext, function (err, pixels) {
                    if (err) {
                        console.log("Bad image path");
                        return
                    }
                    resolve(pixels.shape)
                })
            }).then(async (size) => {
                let calcSize = utils.getPHSize(size[size.length - 3], size[size.length - 2])
                let addRes = await dao_emotion.addEmoticon({
                    userId,
                    createdAt: timestamps,
                    src: src + `?size=${calcSize.width}x${calcSize.height}`
                })
                res.end(JSON.stringify({
                    code: addRes ? 200 : 400,
                    emoticonId: addRes.id,
                    src: src,
                    fileName: file.originalname,
                    size: calcSize,
                }))
            })
        } else {
            res.end(JSON.stringify({
                code: 200,
                msg: 'upload file success',
                src: src,
                fileName: file.originalname,
            }))
        }
    })
}

module.exports = {
    getFileList,
    uploadFile,
};
