// dao_session 和 utils，分别用于处理会话数据访问和其他实用功能。
const dao_session = require('../dao/session')
const utils = require('./utils')

// getSessionList 函数：这个函数是用来获取会话列表的。
// 首先设置响应头的 content-type，然后从请求中获取查询参数，主要是 lastTime，表示上次请求的时间戳。
// 接着调用 dao_session.getSessionList 方法获取会话列表，传入当前用户的 userId 和 lastTime。
// 然后再调用 dao_session.getTotalUnread 方法获取当前用户的总未读消息数量。
// 最后返回一个 JSON 格式的响应，包括一个 code 字段表示请求状态，一个 list 字段表示会话列表，以及一个 total 字段表示总未读消息数量。

// 获取session列表
const getSessionList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req)
    // if (!user) {
    //   res.end(JSON.stringify({
    //     code: 401,
    //     msg: '未登录',
    //   }))
    //   return
    // }
    const query = req.query;
    // 以时间戳为分页标识，因为以页数的话可能会导致新增会话后出现重复的情况
    const list = await dao_session.getSessionList(req.session["userId"], query.lastTime)
    const total = await dao_session.getTotalUnread(req.session["userId"])
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list,
        total,
    }))
}

// updateSession 函数：这个函数用于更新通知或群组的会话。
// 首先判断会话是否存在，然后调用 dao_session.updateSession 方法更新会话信息，包括最后一条消息内容、时间戳等。
// 如果 receiverId 为 -1，则表示这是系统通知，会将 isNoti 设置为 true。
// 最后返回更新操作的结果。

// 更新通知或群组的session
const updateSession = async (userId, receiverId, lastChat, timestamps) => {
    // 先判断是否存在会话
    let sessionId = await dao_session.getSessionExist(userId, receiverId);
    let res1 = await dao_session.updateSession({sessionId, lastChat, timestamps, userId, isNoti: receiverId == -1})
    // 第三个参数是type  如果receiverId为 -1  则说明type为系统通知
    // let res2 = await dao_session.updateSessionReadNum(sessionId, userId, receiverId == -1 && 2)
    // return res1 && res2 && sessionId
    return res1 && sessionId
}

// resetUnread 函数：这个函数用于清空会话的未读消息数量。
// 首先判断是否传入了必要的参数 sessionId，然后调用 dao_session.resetUnread 方法重置未读消息数量。
// 最后返回一个 JSON 格式的响应，包括一个 code 字段表示操作状态，以及一个 msg 字段表示操作结果。

// 清空session未读数量
const resetUnread = async (req, res, next) => {
    // 判断是否传参
    if (!req.query.sessionId) {
        res.end(JSON.stringify({
            code: 400,
            msg: '缺少必要参数: sessionId',
        }))
        return
    }
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req)
    // if (!user) {
    //   res.end(JSON.stringify({
    //     code: 401,
    //     msg: '未登录',
    //   }))
    //   return
    // }
    let resetRes = await dao_session.resetUnread(req.query.sessionId, req.session["userId"])
    res.end(JSON.stringify({
        code: resetRes ? 200 : 400,
        msg: resetRes ? 'success' : 'fail'
    }))
}

module.exports = {
    getSessionList,
    updateSession,
    resetUnread,
};
