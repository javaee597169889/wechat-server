// 通过 require 关键字引入了 ../dao/notification 模块和 ./utils 模块，分别赋值给 dao_notification 和 utils 变量。
// 这些模块可能包含了与数据库交互和一些通用工具函数相关的功能。
const dao_notification = require('../dao/notification')
const utils = require('./utils')
// const socket_user = require('../websocket/user')

// 定义了一个名为 getNotificationList 的异步函数，接受三个参数：req、res 和 next，这是一个典型的 Express 中间件函数签名。
// 在函数内部，首先设置响应头 content-type 为 application/json;charset=utf-8，以确保返回的数据类型为 JSON 格式，并且指定了字符编码为 UTF-8。
// 注释掉了一段代码，该段代码似乎是用于验证用户登录状态的，通过调用 utils.isLogin 函数来验证用户是否已登录，如果用户未登录，则提前结束函数执行。
// 获取请求中的查询参数 query，如果请求中未提供 lastTime 参数，则默认使用当前时间的时间戳作为 lastTime。
// 调用 dao_notification.getNotificationList 函数，传入当前用户的用户 ID（通过 req.session["userId"] 获取）和 lastTime 参数，以获取通知列表数据。
// 将获取到的通知列表数据组装成一个 JSON 对象，包含一个 code 属性表示请求状态，200 表示成功，400 表示失败；以及一个 list 属性，存放通知列表数据。
// 将组装好的 JSON 对象通过 res.end 方法返回给客户端。

// 获取聊天记录
const getNotificationList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const query = req.query;
    query.lastTime = query.lastTime || new Date().getTime()
    const list = await dao_notification.getNotificationList(req.session["userId"], query.lastTime)
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

// 通过 module.exports 导出了 getNotificationList 函数，使其可以被其他模块引用和使用
module.exports = {
    getNotificationList,
};
