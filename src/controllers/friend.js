// dao_friend_request: 用于处理好友请求相关的数据库操作。
// dao_friend: 用于处理好友关系相关的数据库操作。
// dao_session: 用于处理会话相关的数据库操作。
// dao_chat: 用于处理聊天相关的数据库操作。
// dao_user: 用于处理用户相关的数据库操作。
// utils: 包含一些通用的工具函数。
// socket_user: 用于处理 WebSocket 相关的用户操作。

// 引入了几个模块并导出了它们，这些模块用于处理与好友请求、好友关系、会话、聊天和用户相关的数据库操作，以及一些通用的工具函数和 WebSocket 相关操作。
const dao_friend_request = require('../dao/friend_request')
const dao_friend = require('../dao/friend')
const dao_session = require('../dao/session')
const dao_chat = require('../dao/chat')
const dao_user = require('../dao/user')
const utils = require('./utils')
const socket_user = require('../websocket/user')

// 首先，它从请求中获取当前用户的 userId 和查询参数。然后，它通过查询参数中的 requestId 获取好友请求信息，并检查该请求是否存在以及当前用户是否已经是请求中的用户的好友。
// 如果请求不存在或者已经是好友，则返回相应的错误信息。
// 接着，它通过调用相应的数据访问对象（DAO）的方法，将请求中的用户添加为当前用户的好友，并更新好友请求的状态。
// 随后，它获取好友的用户信息，并返回给客户端。
// 在添加好友成功后，它会进行一系列的操作，包括创建或获取会话 ID，添加聊天记录，以及通过 WebSocket 向当前用户和好友发送聊天消息。
// 最后，它返回一个 JSON 格式的响应，包含添加好友的结果以及好友的用户信息

// 添加好友
const addFriend = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const userId = req.session["userId"]
    // 获取请求参数
    const query = req.query;
    // 获取好友请求
    let friendReq = await dao_friend_request.getRequestById(query.requestId)
    if (!friendReq) {
        res.end(JSON.stringify({
            code: 400,
            msg: '该请求不存在',
        }))
        return
    }
    // 判断是否已是好友
    const isFriend = await dao_friend.getIsFriend(friendReq.friendId, friendReq.userId)
    if (isFriend) {
        res.end(JSON.stringify({
            code: 400,
            msg: '该用户已是你的好友',
        }))
        return
    }
    const add_res = await dao_friend.addFriend(userId, friendReq.friendId)
    const update_res = await dao_friend_request.updateRequest(query.requestId, 1, 1)
    // 获取好友用户信息
    let friendInfo = await dao_user.getUserById(friendReq.friendId)
    res.end(JSON.stringify({
        code: add_res && update_res ? 200 : 400,
        // msg: 'Add friend ' + add_res && update_res ? 'success' : 'fail'
        userInfo: add_res && update_res && friendInfo
    }))
    let timestamps = new Date().getTime()
    // 判断会话列表是否存在
    let sessionId = await dao_session.getSessionExist(friendReq.friendId, friendReq.userId)
    if (!sessionId) {
        // 如果两个用户之间不存在sessionId 创建session会话窗口
        sessionId = await dao_session.createSession(friendReq.friendId, friendReq.userId, friendReq.content, timestamps)
    }
    // 添加聊天记录
    let chat = {
        sessionId: sessionId,
        senderId: friendReq.friendId,
        receiverId: friendReq.userId,
        content: friendReq.content,
        type: 0,
        updatedAt: timestamps,
    }
    let chatRes = await dao_chat.addChat(chat)
    if (!chatRes) console.log('聊天记录添加失败');
    // websocket推送消息
    // 推送给自己
    let self = socket_user.getUserById(friendReq.userId)
    self && process.io.to(self.socketId).emit('chat', {
        content: friendReq.content,
        time: timestamps,
        sessionId,
        talkerId: friendReq.friendId,
        type: 0,
    })
    // 推送给friend
    let friend = socket_user.getUserById(friendReq.friendId)
    friend && process.io.to(friend.socketId).emit('chat', {
        content: friendReq.content,
        time: timestamps,
        sessionId,
        talkerId: friendReq.userId,
        type: 0,
    })
}

// 这段代码是一个异步函数，用于获取用户的好友列表。
// 它首先设置响应头的 content-type 为 application/json，并使用 dao_friend 模块的 getFriendList 方法获取当前用户的好友列表。
// 然后，它将获取到的好友列表封装成一个 JSON 格式的响应返回给客户端，包含了请求的结果状态码和好友列表信息。

// 获取好友列表
const getFriendList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const list = await dao_friend.getFriendList(req.session["userId"])
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

// 首先，设置响应头的 content-type 为 application/json，并检查请求中是否包含必要参数 friendId。
// 如果缺少 friendId 参数，则返回一个 JSON 格式的错误响应，提示缺少必要参数，并终止函数执行。
// 接着，获取当前用户的 userId，并通过 dao_friend 模块的 getIsFriend 方法判断当前用户是否与待删除的好友存在好友关系。
// 如果不存在好友关系，则返回一个 JSON 格式的错误响应，提示删除失败，并终止函数执行。
// 如果存在好友关系，则通过 dao_friend 模块的 deleteFriend 方法删除好友关系。
// 如果删除失败，则返回一个 JSON 格式的错误响应，并终止函数执行。
// 更新会话信息：首先，获取会话的 sessionId，并更新会话的最后一条消息为 '[解除好友关系通知]'，并标记为通知类型。
// 然后，将删除好友的消息保存到聊天记录中。
// 使用 WebSocket 推送消息通知：首先，向当前用户推送一条通知消息，告知其与对方解除了好友关系。
// 然后，向对方好友推送一条通知消息，告知其与当前用户解除了好友关系。
// 最后，返回一个 JSON 格式的成功响应，提示删除好友成功。

// 删除好友的回调
const deleteFriend = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    if (!req.query.friendId) {
        res.end(JSON.stringify({
            code: 400,
            msg: '缺少必要参数: friendId',
        }))
        return
    }
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    let userId = req.session["userId"]
    // 判断是否是好友
    let isFriend = await dao_friend.getIsFriend(userId, req.query.friendId)
    if (!isFriend) {
        res.end(JSON.stringify({
            code: 400,
            msg: '删除失败, 您与对方已经不是好友关系',
        }))
        return
    }
    let deleteRes = await dao_friend.deleteFriend(userId, req.query.friendId)
    if (!deleteRes) {
        res.end(JSON.stringify({
            code: 400,
            msg: '删除失败',
        }))
        return
    }
    let timestamps = new Date().getTime()
    let sessionId = await dao_session.getSessionExist(userId, req.query.friendId)
    await dao_session.updateSession({
        sessionId,
        lastChat: '[解除好友关系通知]',
        timestamps,
        userId: userId,
        isNoti: true
    })
    // await dao_session.updateSessionReadNum(sessionId, user.id)
    // 保存删除好友信息至聊天记录中
    let chat = {
        sessionId: sessionId,
        senderId: userId,
        receiverId: req.query.friendId,
        content: 'deleteFriend',
        type: 4,
        updatedAt: timestamps,
    }
    let chatRes = await dao_chat.addChat(chat)
    if (!chatRes) console.log('聊天记录添加失败');
    // websocket推送消息
    // 推送给自己
    let self = socket_user.getUserById(userId)
    self && process.io.to(self.socketId).emit('chat', {
        content: '您已与对方解除好友关系',
        time: timestamps,
        sessionId,
        talkerId: req.query.friendId,
        type: 4,
    })
    // 推送给friend
    let friend = socket_user.getUserById(req.query.friendId)
    friend && process.io.to(friend.socketId).emit('chat', {
        content: '对方已与您解除好友关系',
        time: timestamps,
        sessionId,
        talkerId: req.query.friendId,
        type: 4,
    })
    res.end(JSON.stringify({
        code: 200,
        msg: '删除好友成功'
    }))
}

module.exports = {
    addFriend,
    getFriendList,
    deleteFriend,
};
