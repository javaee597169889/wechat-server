const dao_user = require('../dao/user')
const dao_session = require('../dao/session')
const md5 = require('md5');
const utils = require('./utils')

// signin:
// 该函数用于处理用户注册请求。
// 首先，它从请求体中获取用户名、密码、邮箱和头像信息。
// 然后，检查用户名和密码是否为空，如果为空则返回错误信息。
// 接着，查询数据库，判断用户是否已经存在，如果存在则返回用户已存在的错误信息。
// 如果用户不存在，则将用户信息写入数据库，并创建系统通知会话。
// 最后，返回注册成功的消息和用户信息。
// 注册用户
const signin = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    const {userName, password, email, avatar} = req.body;
    // 先判断用户名和密码是否为空
    if (!userName || !password) {
        res.end(JSON.stringify({
            code: 400,
            msg: 'username和password不能为空'
        }))
        return
    }
    // 查询用户是否存在
    let isExisted = await dao_user.isExisted(userName);
    if (isExisted) {
        res.end(JSON.stringify({
            code: 400,
            msg: '用户已存在'
        }))
        return
    }
    let timestamps = new Date().getTime()
    // 写入数据库
    let user = await dao_user.addUser({
        userName,
        password: md5(password),
        email,
        avatar,
        createdAt: timestamps,
        updatedAt: timestamps
    });
    user = user.get({plain: true})
    if (user.id) {
        // 自动创建系统通知会话
        await dao_session.createSession(user.id, -1, '', timestamps, 2)
        // 返回响应
        res.end(JSON.stringify({
            code: 200,
            msg: '用户注册成功',
            userInfo: {
                id: user.id,
                userName,
                email,
                avatar,
            }
        }))
    }
}

// login:
// 该函数用于处理用户登录请求。
// 首先，从请求体中获取用户名和密码。
// 然后，检查用户名和密码是否为空，如果为空则返回错误信息。
// 接着，查询数据库，判断用户是否存在，如果不存在则返回用户不存在的错误信息。
// 如果用户存在，再验证密码是否匹配，如果不匹配则返回密码错误的消息。
// 如果用户名和密码都正确，则将用户信息写入 session 中，表示用户已登录。
// 最后，返回登录成功的消息和用户信息。
// 用户登录
const login = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    const {userName, password} = req.body;
    // 先判断用户名和密码是否为空
    if (!userName || !password) {
        res.end(JSON.stringify({
            code: 400,
            data: '用户名和密码不能为空'
        }))
        return
    }
    // 查询用户是否存在
    let user = await dao_user.isExisted(userName);
    if (!user) {
        res.end(JSON.stringify({
            code: 400,
            msg: '用户不存在',
        }))
        return
    }
    if (user.password !== md5(password)) {
        res.end(JSON.stringify({
            code: 400,
            msg: '密码错误',
        }))
        return
    }
    // 登录成功
    // 写入session
    req.session["userId"] = user.id
    // 返回结果
    res.end(JSON.stringify({
        code: 200,
        msg: "登录成功",
        userInfo: {
            id: user.id,
            userName: user.userName,
            email: user.email,
            avatar: user.avatar,
            createdAt: user.createdAt
        }
    }))
}

// getLogin:
// 该函数用于获取当前登录用户的信息。
// 首先，从 session 中获取用户的 ID。
// 然后，根据用户的 ID 查询数据库获取用户信息。
// 最后，返回登录成功的消息和用户信息。
// 判断用户是否是登录状态
const getLogin = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await utils.isLogin(req, res)
    const user = await dao_user.getUserById(req.session["userId"])
    // 返回结果
    res.end(JSON.stringify({
        code: 200,
        msg: "登录成功",
        userInfo: {
            id: user.id,
            userName: user.userName,
            email: user.email,
            avatar: user.avatar,
            createdAt: user.createdAt
        }
    }))
}

// logout:
// 该函数用于处理用户退出登录请求。
// 首先，删除 session 中存储的用户 ID，表示用户已退出登录。
// 最后，返回退出成功的消息。
// 退出登录
const logout = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    delete req.session.userId
    res.end(JSON.stringify({
        code: 200,
        msg: '退出成功'
    }))
}

// getUserInfo:
// 该函数用于获取指定用户的信息。
// 首先，从查询参数中获取要查询用户的 ID。
// 然后，根据用户的 ID 查询数据库获取用户信息。
// 最后，返回用户的信息。
// 查询用户
const getUserInfo = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    if (!req.query.userId) {
        res.end(JSON.stringify({
            code: 400,
            msg: "缺少必要参数: userId",
        }))
        return
    }
    // const user = await utils.isLogin(req, res)
    // if (!user) return
    const userInfo = await dao_user.getUserById(req.query.userId)
    // return userInfo
    let {id, userName, avatar, email} = userInfo
    res.end(JSON.stringify({
        code: 200,
        userInfo: {id, userName, avatar, email},
    }))
}

module.exports = {
    signin,
    login,
    getLogin,
    logout,
    getUserInfo,
};
