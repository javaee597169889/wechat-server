// getPHSize 函数接受四个参数：
// width: 图片的宽度。
// height: 图片的高度。
// maxWidth: 允许的最大宽度，默认为 200。
// minWidth: 允许的最小宽度，默认为 50。
// maxHeight: 允许的最大高度，默认为 200。
// minHeight: 允许的最小高度，默认为 50。

// 函数内部根据传入的宽高以及最大、最小宽高值进行计算和调整，以保证宽高在合理的范围内，并且保持原始图片的比例不变。
// 首先，如果图片的宽度超过了最大宽度，则按比例缩放高度，将宽度调整为最大宽度。
// 接着，如果图片的高度超过了最大高度，则按比例缩放宽度，将高度调整为最大高度。
// 然后，如果图片的宽度小于最小宽度，则按比例缩放高度，将宽度调整为最小宽度，并且如果调整后的高度超过了最大高度，则将高度调整为最大高度。
// 最后，如果图片的高度小于最小高度，则按比例缩放宽度，将高度调整为最小高度，并且如果调整后的宽度超过了最大宽度，则将宽度调整为最大宽度。
// 函数返回一个对象，包含调整后的宽度和高度，均取整数值。

// 计算占位图的宽高
const getPHSize = function (width, height, maxWidth = 200, minWidth = 50, maxHeight = 200, minHeight = 50) {
    if (width > maxWidth) {
        height *= maxWidth / width
        width = maxWidth
    }
    if (height > maxHeight) {
        width *= maxHeight / height
        height = maxHeight
    }
    if (width < minWidth) {
        height *= minWidth / width
        width = minWidth
        height = height > maxHeight ? maxHeight : height
    }
    if (height < minHeight) {
        width *= minHeight / height
        height = minHeight
        width = width > maxWidth ? maxWidth : width
    }
    return {width: Math.ceil(width), height: Math.ceil(height)}
}

// 通过 module.exports 导出了 getPHSize 函数，使其可以被其他模块引用和使用
module.exports = {
    getPHSize,
};
