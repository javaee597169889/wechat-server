// 引入了两个模块，dao_emoticon 用于处理自定义表情数据的访问，utils 则是一个工具模块，可能包含一些常用的功能函数
const dao_emoticon = require('../dao/emoticon')
const utils = require('./utils')
// const socket_user = require('../websocket/user')

// getEmoticonList 函数：这个函数用于获取当前用户的自定义表情列表。
// 首先设置响应头的 content-type，然后调用 dao_emoticon.getEmoticonList 方法获取当前用户的自定义表情列表。
// 最后返回一个 JSON 格式的响应，包括一个 code 字段表示请求状态，以及一个 list 字段表示自定义表情列表。

// 获取自定义表情包
const getEmoticonList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    const list = await dao_emoticon.getEmoticonList(req.session["userId"])
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

// addEmoticon 函数：这个函数用于添加自定义表情。
// 首先设置响应头的 content-type，然后检查请求中是否包含必要的参数 src，如果没有则返回一个错误响应。
// 接着调用 dao_emoticon.addEmoticon 方法添加自定义表情，传入当前用户的 userId、当前时间戳和表情的 src。
// 最后返回一个 JSON 格式的响应，包括一个 code 字段表示添加操作的状态，一个 id 字段表示新添加表情的 ID，以及一个 src 字段表示新添加表情的路径。

// 添加自定义表情   上传表情集成在上传文件中了
const addEmoticon = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    if (!req.query.src) {
        res.end(JSON.stringify({
            code: 400,
            msg: '缺少必要参数: src'
        }))
        return
    }
    let addRes = await dao_emoticon.addEmoticon({
        userId: req.session["userId"],
        createdAt: new Date().getTime(),
        src: req.query.src,
    })
    res.end(JSON.stringify({
        code: addRes ? 200 : 400,
        id: addRes.id,
        src: req.query.src,
    }))
}

module.exports = {
    getEmoticonList,
    addEmoticon,
};
