// 引入了一个模块 dao_chat，用于处理聊天记录的数据访问
const dao_chat = require('../dao/chat')
// const dao_user = require('../dao/user')
// const socket_user = require('../websocket/user')

// getChatList 函数：这个函数用于获取指定会话的聊天记录列表。
// 首先设置响应头的 content-type，然后从请求中获取查询参数，主要是 sessionId 和 lastTime。
// 如果没有传入 sessionId 参数，则返回一个 JSON 格式的错误响应。
// 接着，如果没有传入 lastTime 参数，则将其设置为当前时间戳。
// 然后调用 dao_chat.getChatList 方法获取指定会话在指定时间之后的聊天记录列表。
// 最后返回一个 JSON 格式的响应，包括一个 code 字段表示请求状态，以及一个 list 字段表示聊天记录列表。

// 获取聊天记录
const getChatList = async (req, res, next) => {
    res.set('content-type', 'application/json;charset=utf-8')
    // const user = await dao_user.getUserById(userId)
    const query = req.query;
    if (!query.sessionId) {
        res.end(JSON.stringify({
            code: 400,
            msg: '缺少必要参数: sessionId',
        }))
        return
    }
    query.lastTime = query.lastTime || new Date().getTime()
    const list = await dao_chat.getChatList(query.sessionId, query.lastTime)
    res.end(JSON.stringify({
        code: list ? 200 : 400,
        list: list
    }))
}

module.exports = {
    getChatList,
};
