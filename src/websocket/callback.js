// 通过 require("../dao/session") 引入了 dao_session 模块，该模块应该包含了与会话相关的数据库操作方法
const dao_session = require("../dao/session");

// resetUnread 函数接受一个参数 params，这个参数应该是一个对象，包含了两个属性：sessionId（会话 ID）和 userId（用户 ID）。
// 在函数内部，通过调用 dao_session 模块的 resetUnread 方法，并传入 params.sessionId 和 params.userId 作为参数，来重置指定会话的未读消息计数。
// 该函数没有返回值，它的作用是重置指定会话的未读消息计数。

// socket请求方法
const resetUnread = function (params) {
    dao_session.resetUnread(params.sessionId, params.userId)
}

// 通过 module.exports 将 resetUnread 函数暴露出去，以便其他模块可以引入并调用
module.exports = {
    resetUnread,
};
