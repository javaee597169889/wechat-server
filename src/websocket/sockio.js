// const socket = {}：定义了一个空对象 socket，可能是用于存储 Socket 相关的信息
const socket = {}
// 引入了 socket.io 模块，用于创建 Socket.io 服务器。通过 const socketio = require('socket.io') 将其赋值给 socketio 变量
const socketio = require('socket.io')
// 引入了之前定义的用户管理模块 user.js 中导出的函数，包括 userJoin、getCurrentUser、getUserById 和 removeUser
const {userJoin, getCurrentUser, getUserById, removeUser} = require('./user')
// 引入了 fs（文件系统）模块，用于处理文件系统相关操作。
const fs = require('fs')
// 引入了 path 模块，用于处理文件路径相关操作。
const path = require('path')
// 引入了与文件操作相关的 DAO（数据访问对象）模块 dao_file
const dao_file = require('../dao/file')
const utils = require('./utils')
const getPixels = require("get-pixels")
// 引入了其他 DAO 模块，包括 dao_chat、dao_user、dao_session、dao_notification 和 dao_friend，用于与数据库进行交互。
const dao_chat = require('../dao/chat')
const dao_user = require('../dao/user')
const dao_session = require('../dao/session')
const dao_notification = require('../dao/notification')
const dao_friend = require('../dao/friend')
// 引入了控制器模块 control_session 和回调模块 callback
const control_session = require('../controllers/session')
const callback = require('./callback')

// 定义了一个名为getSocket的函数，该函数接受一个服务器对象作为参数，并返回一个socket.io实例
function getSocket(server) {
    // 创建一个socket.io实例，并将其绑定到传入的服务器对象。
    // 同时设置CORS（跨源资源共享）为true，以便允许跨域请求，以及设置最大HTTP缓冲区大小为3MB。
    const io = socketio(server, {
        cors: true,
        maxHttpBufferSize: 3 * 1024 * 1024,
    })
    // 为connection事件定义一个回调函数，该函数将在每个客户端连接时被调用。
    // 连接
    io.on('connection', async socket => {
        // 在该回调函数中，首先获取当前时间戳，并根据客户端的session信息获取用户ID和用户信息。
        let timestamps = new Date().getTime()
        // 根据session获取用户id，查询用户信息
        let userInfo = socket.handshake.session.userId && await dao_user.getUserById(socket.handshake.session.userId)
        // 如果用户未登录，则向客户端发送一个登录状态为logout的消息，并断开连接。
        if (!userInfo) {
            console.log('用户未登录');
            socket.emit('message', {
                msgType: 'login',
                status: 'logout',
                time: timestamps
            })
            // 为true时彻底断开连接，否则只是断开命名空间
            socket.disconnect(true)
            return
        }
        // socket.on('login', async () => {
        // 如果用户已登录，则检查该用户是否已存在于在线用户列表中。
        // 判断用户是否存在
        let oldUser = getUserById(userInfo.id)
        // 如果已存在，则向其发送一个登录状态为logout的消息，并将其从在线用户列表中移除。
        if (oldUser) {
            io.to(oldUser.socketId).emit('message', {
                msgType: 'login',
                status: 'logout',
                time: timestamps
            })
            removeUser(oldUser.socketId)
        }
        // 然后，将当前用户添加到在线用户列表中，并向其发送一个登录状态为success的消息。
        const user = userJoin(socket.id, userInfo);
        // 发送给当前客户端
        // socket.emit('userInfo', user)
        socket.emit('message', {
            msgType: 'login',
            status: 'success',
            content: '登录通知',
            time: timestamps
        })
        // 保存通知记录
        await dao_notification.addNotification(userInfo.id, 0, timestamps)
        // 更新session
        await control_session.updateSession(userInfo.id, -1, '登录通知', timestamps)
        // 为chat事件定义一个回调函数，该函数将在客户端发送聊天消息时被调用。
        // 聊天
        socket.on('chat', async (data) => {
            // const user = await getCurrentUser(socket.id)
            const timestamps = new Date().getTime()
            // if (!user) {
            //   socket.emit('message', {
            //     msgType: 'chat',
            //     status: 'fail',
            //     uuid: data.uuid,
            //     time: timestamps,
            //     content: '用户不存在',
            //   })
            //   return;
            // }
            // 在该回调函数中，首先检查发送方和接收方是否为好友关系。
            // 如果不是好友关系，则向发送方发送一个聊天状态为fail的消息。
            // 判断是否是好友关系
            let isFriend = await dao_friend.getIsFriend(user.id, data.receiverId)
            if (!isFriend) {
                socket.emit('message', {
                    sessionId,
                    msgType: 'chat',
                    status: 'fail',
                    uuid: data.uuid,
                    time: timestamps,
                    content: '对方与您不是好友关系!',
                })
                return;
            }
            // 如果为好友关系，则根据消息类型处理消息内容，并将其存储在文件系统或数据库中。
            let content = null;
            // data.type  0:文字    1:图片     2:文件      3: 自定义表情    4: 通知   5: 视频邀请
            // 文字聊天 或 自定义表情
            if (data.type == 0 || data.type == 3 || data.type == 5) {
                content = data.content
            } else if (data.type == 1 || data.type == 2) {
                // 存储文件
                // 判断是否存在文件夹,没有就创建
                fs.existsSync("../uploadFile") || fs.mkdirSync("../uploadFile")
                let ext = path.extname(data.fileInfo.fileName)
                let dir = path.join('../uploadFile/', timestamps + ext)
                // 写入文件
                fs.writeFileSync(dir, data.content)
                let src = 'http://localhost:3000/' + timestamps + ext
                await new Promise((resolve, reject) => {
                    // 判断是否是图片
                    if (data.type == 1) {
                        getPixels(dir, (err, pixels) => {
                            if (err) {
                                console.log("Bad image path");
                                return
                            }
                            // 计算占位图的宽高
                            // let width = pixels.shape.length === 3 ? pixels.shape[0]:pixels.shape[1]
                            // let height = pixels.shape.length === 3 ? pixels.shape[1]:pixels.shape[2]
                            let length = pixels.shape.length
                            let size = utils.getPHSize(pixels.shape[length - 3], pixels.shape[length - 2])
                            content = src + '?size=' + size.width + 'x' + size.height
                            resolve(1)
                        })
                    } else {
                        content = src
                        resolve(2)
                    }
                }).then((type) => {
                    // 然后，将聊天记录添加到数据库中，并向发送方和接收方发送相应的消息。
                    // 将上传记录存入数据库
                    dao_file.addFile({
                        userId: user.id,
                        size: data.fileInfo.size,
                        fileName: data.fileInfo.fileName,
                        src,
                        createdAt: timestamps,
                        type,
                    })
                })
            }
            // 更新会话列表
            let sessionId = await dao_session.updateSession({
                sessionId: data.sessionId,
                lastChat: content.substr(0, 100),
                timestamps,
                userId: user.id
            })
            // 将聊天记录存入数据库
            let chat = {
                sessionId: sessionId,
                senderId: user.id,
                receiverId: data.receiverId,
                content: content,
                type: data.type,
                updatedAt: timestamps,
                others: data.type == 0 ? undefined : JSON.stringify(data.fileInfo)
            }
            let chatRes = await dao_chat.addChat(chat)
            if (!chatRes) console.log('聊天记录添加失败');
            // 返回给发送方
            socket.emit('message', {
                msgType: 'chat',
                status: 'success',
                // userInfo: user,
                // 这里不用time 和数据库字段保持一致
                updatedAt: timestamps,
                content: content,
                uuid: data.uuid,
                sessionId,
                talkerId: data.receiverId,
                type: data.type,
            })
            // 判断用户是否在线
            const receiver = await getUserById(data.receiverId)
            if (receiver) {
                // 发送至接收方
                io.to(receiver.socketId).emit('chat', {
                    // userInfo: user,
                    content: content,
                    // 这里不用time 和数据库字段保持一致
                    updatedAt: timestamps,
                    sessionId,
                    talkerId: user.id,
                    type: data.type,
                    others: data.fileInfo && JSON.stringify(data.fileInfo),
                })
            }
        })
        // 为request事件定义一个回调函数，该函数将在客户端发送请求时被调用
        // 在该回调函数中，根据请求的函数名称和参数调用相应的回调函数。
        // 调用callback中的方法
        socket.on('request', async (data) => {
            callback[data.function] && callback[data.function](data.params)
        })
        // 为webRTC事件定义一个回调函数，该函数将在客户端发送WebRTC请求时被调用。
        // 在该回调函数中，将请求转发给接收方。
        socket.on('webRTC', async (data) => {
            const receiver = await getUserById(data.receiverId)
            if (!receiver) return
            io.to(receiver.socketId).emit('webRTC', data)
        })
        // 为disconnect事件定义一个回调函数，该函数将在客户端断开连接时被调用。
        // 在该回调函数中，将用户从在线用户列表中移除，并打印断开连接的原因。
        // 断开连接
        socket.on('disconnect', (reason) => {
            console.log("id为" + socket.id + "的用户端口断开……断开原因：" + reason);
            removeUser(socket.id)
        })
    })
    return io
}

// 定义了一个名为 getSocket 的函数，并将其赋值给了 socket 对象的 getSocket 属性。
// 这意味着可以通过 socket.getSocket() 来调用 getSocket 函数。
socket.getSocket = getSocket;

// 将 socket 对象导出，使得其他文件在引入该模块时可以访问到 socket 对象及其属性
//导出socket
module.exports = socket;
