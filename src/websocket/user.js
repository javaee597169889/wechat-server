// 定义了一个空数组 users，用于存储当前在线的用户信息
const users = [];

// getCurrentUser(socketId) 函数用于根据提供的 socketId 获取当前用户的信息。
// 它通过调用数组的 find 方法查找与给定 socketId 相匹配的用户，并返回该用户的信息。
// get current user
function getCurrentUser(socketId) {
    return users.find(user => user.socketId === socketId)
}

// userJoin(socketId, userInfo) 函数用于将用户加入聊天。它
// 接受两个参数，socketId 是用户的 Socket ID，userInfo 是用户的其他信息。
// 该函数首先创建一个包含 socketId 和 userInfo 的新用户对象，然后将其添加到 users 数组中，并最终返回添加的用户对象。
// join user to chat
function userJoin(socketId, userInfo) {
    const user = Object.assign({socketId}, userInfo)
    users.push(user)
    return user;
}

// getUserById(id) 函数用于根据用户 ID 获取用户信息。
// 它通过调用数组的 find 方法查找与给定用户 ID 相匹配的用户，并返回该用户的信息。
function getUserById(id) {
    return users.find(user => user.id === id * 1)
}

// removeUser(socketId) 函数用于移除用户。
// 它接受一个参数 socketId，表示要移除的用户的 Socket ID。
// 函数首先查找 users 数组中与给定 socketId 相匹配的用户的索引，然后使用 splice 方法将该用户从数组中移除。
// remove user
function removeUser(socketId) {
    let index = users.findIndex(item => item.socketId === socketId)
    if (index != -1) users.splice(index, 1)
    console.log(index, users);
}

// 通过 module.exports 将这些函数暴露出去，以便其他模块可以引入并使用。
module.exports = {
    userJoin,
    getCurrentUser,
    getUserById,
    removeUser,
};
