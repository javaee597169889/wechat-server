// 函数接受四个参数：width（原始图像的宽度）、height（原始图像的高度）、maxWidth（占位图的最大宽度，默认为 200px）、minWidth（占位图的最小宽度，默认为 50px）、maxHeight（占位图的最大高度，默认为 200px）、minHeight（占位图的最小高度，默认为 50px）。
// 首先，函数检查原始图像的宽度是否大于最大宽度 maxWidth，如果是，则按比例缩小高度，使其适应最大宽度，并将宽度设为最大宽度。
// 接着，函数检查缩小后的高度是否大于最大高度 maxHeight，如果是，则按比例缩小宽度，使其适应最大高度，并将高度设为最大高度。
// 然后，函数检查缩小后的宽度是否小于最小宽度 minWidth，如果是，则按比例增加高度，使其达到最小宽度，并将宽度设为最小宽度，同时确保高度不超过最大高度。
// 最后，函数检查缩小后的高度是否小于最小高度 minHeight，如果是，则按比例增加宽度，使其达到最小高度，并将高度设为最小高度，同时确保宽度不超过最大宽度。
// 返回计算后的宽度和高度，以对象的形式 { width, height } 返回，并使用 Math.ceil 方法将结果取整，确保返回的是整数值。

// 计算占位图的宽高
const getPHSize = function (width, height, maxWidth = 200, minWidth = 50, maxHeight = 200, minHeight = 50) {
    if (width > maxWidth) {
        height *= maxWidth / width
        width = maxWidth
    }
    if (height > maxHeight) {
        width *= maxHeight / height
        height = maxHeight
    }
    if (width < minWidth) {
        height *= minWidth / width
        width = minWidth
        height = height > maxHeight ? maxHeight : height
    }
    if (height < minHeight) {
        width *= minHeight / height
        height = minHeight
        width = width > maxWidth ? maxWidth : width
    }
    return {width: Math.ceil(width), height: Math.ceil(height)}
}

module.exports = {
    getPHSize,
};
