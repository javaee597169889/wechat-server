// 引入了 Friend 和 User 模型
const Friend = require('../models/friend.js')
const User = require('../models/user.js')

// 定义了一个名为 getIsFriend 的异步函数，用于判断两个用户是否为好友关系。
// 它接受两个参数 userId 和 friendId，分别表示用户 ID 和好友 ID。
// 在函数内部，使用 Friend.findOne() 方法查询数据库，条件是用户 ID 等于 userId 且好友 ID 等于 friendId。
// 如果查询到了结果，表示两个用户已经是好友，函数返回 true；否则返回 false。
// 判断好友是否存在
const getIsFriend = async (userId, friendId) => {
    const res = await Friend.findOne({
        where: {
            // id: id
            userId: userId,
            friendId: friendId
        },
        raw: true,
    })
    return Boolean(res)
}

// 定义了一个名为 addFriend 的异步函数，用于添加好友关系。
// 它接受两个参数 uid1 和 uid2，分别表示两个用户的 ID。
// 在函数内部，首先获取当前时间戳作为创建时间，并使用 Friend.create() 方法分别向数据库中添加两条记录，表示两个用户之间的好友关系。
// 如果两条记录都成功添加，则返回 true，否则返回 false。
// 添加好友
const addFriend = async (uid1, uid2) => {
    let timestamps = new Date().getTime()
    const res1 = await Friend.create({
        userId: uid1,
        friendId: uid2,
        createdAt: timestamps,
        updatedAt: timestamps,
    })
    const res2 = await Friend.create({
        userId: uid2,
        friendId: uid1,
        createdAt: timestamps,
        updatedAt: timestamps,
    })
    return Boolean(res1 && res2)
}

// 定义了一个名为 getFriendList 的异步函数，用于获取指定用户的好友列表。
// 它接受一个参数 userId，表示用户 ID。在函数内部，首先使用 Friend.belongsTo() 方法将 Friend 模型与 User 模型关联起来，以便通过外键关系获取好友的详细信息。
// 然后使用 Friend.findAll() 方法查询数据库，条件是用户 ID 等于 userId，并且通过 include 选项关联了 User 模型，以获取好友的相关信息。
// 最后，函数返回查询到的好友列表。
// 获取好友列表
const getFriendList = async (userId) => {
    Friend.belongsTo(User, {foreignKey: 'friendId', targetKey: 'id'});
    return await Friend.findAll({
        where: {userId: userId},
        attributes: ['user.id', 'user.userName', 'user.avatar', 'user.email'],
        include: [{
            model: User,
            attributes: [],
        }],
        raw: true,
    })
}

// 定义了一个名为 deleteFriend 的异步函数，用于删除好友关系。
// 它接受两个参数 uid1 和 uid2，分别表示两个用户的 ID。
// 在函数内部，使用 Friend.destroy() 方法分别删除两条好友关系记录。
// 如果两条记录都成功删除，则返回 true，否则返回 false。
// 删除好友
const deleteFriend = async (uid1, uid2) => {
    const res1 = await Friend.destroy({
        where: {
            userId: uid1,
            friendId: uid2,
        }
    })
    const res2 = await Friend.destroy({
        where: {
            userId: uid2,
            friendId: uid1,
        }
    })
    return Boolean(res1 && res2)
}

// 通过 module.exports 导出了所有定义的函数，以便在其他文件中使用
module.exports = {
    getIsFriend,
    addFriend,
    getFriendList,
    deleteFriend,
}
