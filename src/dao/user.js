// 引入了一个名为 User 的模型，这个模型应该定义了与用户相关的数据库表结构和操作方法
const User = require('../models/user.js')

// where: { userName: userName }：根据用户名进行条件查询。
// raw: true：设置为 true 表示只返回源数据，而不是 Sequelize 实例对象。

// isExisted 函数用于根据用户名判断用户是否存在，如果存在则返回该用户的信息。
// 具体逻辑是通过 User.findOne() 方法在数据库中查找符合条件的记录
// 根据用户名判断用户是否存在, 存在返回用户
const isExisted = async (userName) => {
    return await User.findOne({
        where: {
            userName: userName
        },
        raw: true,
    })
}

// 函数用于向数据库添加用户，它使用 User.create() 方法创建新的用户记录，并将传入的用户信息作为参数
// 添加用户
const addUser = async (userInfo) => {
    return await User.create(userInfo)
}

// getUserById 函数用于根据用户ID获取用户信息，它通过 User.findOne() 方法根据用户ID进行条件查询，并返回符合条件的用户信息。
// 根据id获取用户
const getUserById = async (id) => {
    return await User.findOne({
        where: {
            id: id
        },
        raw: true,
    })
}

// attributes：指定需要返回的字段。
// order：指定按照哪个字段进行排序。
// offset 和 limit：用于分页，指定返回结果的偏移量和数量。
// raw: true：设置为 true 表示只返回源数据，而不是 Sequelize 实例对象。

// list 函数用于获取用户列表，它通过 User.findAll() 方法查询数据库中的用户记录
// 用户列表
const list = async (pagenum, pagesize) => {
    return await User.findAll({
        attributes: ['id', 'userName', 'createdAt', 'avatar', 'email'],
        order: [
            ['id', 'DESC']
        ],
        offset: (pagenum - 1) * pagesize,
        limit: pagesize,
        raw: true, // 设置为 true，即可返回源数据
    })
};

// 如果传入的 userName 是数组，则遍历数组，对每个用户名进行删除操作，并使用 Promise.all() 等待所有删除操作完成后返回结果。
// 如果 userName 不是数组，则直接删除指定用户名的用户记录。

// remove 函数用于删除用户，它可以单独删除一个用户或者批量删除多个用户
// 删除用户
const remove = async (userName) => {
    // 物理删除
    // 判断是否为数组 若为数组则进行批量删除
    if (userName instanceof Array) {
        let arr = [];
        userName.forEach(item => {
            let p = User.destroy({
                where: {
                    userName: item,
                }
            })
            arr.push(p);
        })
        return await Promise.all(arr)
    } else {
        return await User.destroy({
            where: {
                userName: userName,
            },
        })
    }
}

// 通过 module.exports 导出了这些函数，使其可以在其他文件中被引用和调用
module.exports = {
    isExisted,
    addUser,
    // login,
    list,
    remove,
    getUserById,
}
