// 导入了必要的模块和依赖项，包括 Session 和 User 模型，以及 Sequelize 的一些功能和工具函数
const Session = require('../models/session.js')
const User = require('../models/user.js')
const {Op, Sequelize} = require("sequelize");
const utils = require('../utils/utils');
const DB = require('sequelize')

// getSessionExist 函数用于检查给定的两个用户之间是否已经存在会话。
// 它通过在数据库中查找符合条件的会话记录来实现这一目的。
// 判断会话是否存在
const getSessionExist = async (userId, receiverId) => {
    const res = await Session.findOne({
        where: {
            userId: userId,
            receiverId: receiverId,
        },
        // raw: true,
    })
    return res && res.sessionId
}

// createSession 函数用于创建会话。它接受两个用户的 ID，会话的最后一条聊天消息、时间戳和会话类型等参数。
// 在私聊类型的会话中，会创建两条记录，分别表示双方的会话。
// 创建会话
const createSession = async (uid1, uid2, lastChat, timestamps, type = 0) => {
    let sessionId = utils.createSessionId()
    const res1 = await Session.create({
        sessionId,
        userId: uid1,
        receiverId: uid2,
        lastChat: lastChat,
        updatedAt: timestamps,
        type,
    })
    // 如果不是私聊，只需要创建一个session
    if (type !== 0) return res1 ? sessionId : undefined
    const res2 = await Session.create({
        sessionId,
        userId: uid2,
        receiverId: uid1,
        lastChat: lastChat,
        updatedAt: timestamps,
        type
    })
    return res1 && res2 ? sessionId : undefined
}

// updateSession 函数用于更新会话内容和相关信息。
// 根据传入的参数，它可以更新会话的最后一条消息、时间戳等，并且在需要的情况下更新会话的未读消息数量。
// 函数接受一个参数对象，包括会话的 sessionId、最后一条消息 lastChat、时间戳 timestamps、用户ID userId，以及一个可选的布尔值 isNoti，默认为 false。
// 函数首先检查 isNoti 是否为 false，如果是，表示这不是一个通知消息，而是普通的聊天消息。在这种情况下，函数会执行以下操作：
// 首先，它会更新除了当前用户之外的会话记录。它使用 Session.update 方法更新满足特定条件的记录，包括更新最后一条消息、时间戳，并将未读消息数量加一。
// 然后，它会更新当前用户的会话记录，同样更新最后一条消息和时间戳。
// 最后，函数返回更新操作的结果以及会话的 sessionId。
// 如果 isNoti 不为 false，表示这是一个通知消息，则函数只会更新当前用户的会话记录。它执行的操作与上述类似，只是不需要更新其他用户的会话记录。
// 无论是普通消息还是通知消息，函数都会返回更新操作的结果以及会话的 sessionId。
// 更新会话内容和数量，并更新时间
// isNoti: 是否是通知
const updateSession = async ({sessionId, lastChat, timestamps, userId, isNoti = false}) => {
    if (!isNoti) {
        // 更新其他人的session
        const res1 = await Session.update({
            lastChat,
            updatedAt: timestamps,
            read: DB.fn('1 + abs', DB.col('read'))
        }, {
            where: {
                sessionId,
                userId: {
                    [Op.not]: userId
                },
            },
        })
        const res2 = await Session.update({
            lastChat,
            updatedAt: timestamps,
        }, {
            where: {
                sessionId,
                userId
            },
        })
        return res1 && res2 && sessionId
    } else {
        const res = await Session.update({
            lastChat,
            updatedAt: timestamps,
            read: DB.fn('1 + abs', DB.col('read'))
        }, {
            where: {
                sessionId,
                userId
            },
        })
        return res && sessionId
    }
}

// getSessionList 函数用于获取用户的会话列表。它根据用户的 ID 和最后更新时间来查询数据库中的会话记录，并返回相应的会话列表。
// 同时，它还会关联查询用户信息，以获取会话的用户头像、用户名等信息。
// 查询会话列表
const getSessionList = async (userId, lastTime) => {
    Session.belongsTo(User, {foreignKey: 'receiverId', targetKey: 'id'})
    lastTime = lastTime || new Date().getTime()
    let list = await Session.findAll({
        where: {
            userId,
            updatedAt: {
                [Op.lt]: lastTime,
            },
        },
        limit: 20,
        attributes: ['sessionId', 'lastChat', 'updatedAt', 'type', 'receiverId', 'read',
            [Sequelize.col('user.avatar'), 'cover'],
            [Sequelize.col('user.userName'), 'sessionName'],
        ],
        order: [
            ['updatedAt', 'DESC']
        ],
        include: [{
            model: User,
            as: 'user',
            attributes: ['id', 'userName', 'avatar', 'email'],
        }],
        // raw: true,
    })
    // list = list.map(item => item.get({ plain: true }))
    list.find(item => {
        if (item.dataValues.type == 2) {
            item.dataValues.cover = 'http://114.132.235.129:3000/1648365630990.png'
            item.dataValues.sessionName = 'Socket-Chat系统通知'
            return true
        }
    })
    return list
}

// resetUnread 函数用于将指定会话中的未读消息数量重置为零。
// 清空session未读数量
const resetUnread = async (sessionId, userId) => {
    let res = await Session.update({
        read: 0
    }, {
        where: {
            sessionId,
            userId,
        }
    })
    return Boolean(res)
}

// getTotalUnread 函数用于获取用户所有会话的未读消息总数。
// 查看未读条数
const getTotalUnread = async (userId) => {
    let res = await Session.findAll({
        // userId,
        where: {userId,},
        attributes: [
            [DB.fn('SUM', DB.col('read')), 'total']
        ],
        raw: true
    })
    return res[0].total * 1;
}

// 通过 module.exports 将这些函数导出，以便其他模块可以使用它们
module.exports = {
    getSessionExist,
    createSession,
    updateSession,
    getSessionList,
    // updateSessionReadNum,
    resetUnread,
    getTotalUnread,
}
