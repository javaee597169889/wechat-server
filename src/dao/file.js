// 引入了 File 模型和 Sequelize 中的运算符 Op
const File = require('../models/files.js')
const {Op} = require("sequelize");

// 定义了一个名为 addFile 的异步函数，用于添加文件记录。
// 它接受一个参数 file，表示要添加的文件信息。
// 在函数内部，使用 File.create(file) 方法将 file 对象保存到数据库中，并将结果赋值给变量 res。
// 然后，函数返回了一个布尔值，表示文件记录是否成功添加，这里使用 Boolean(res) 将 Sequelize 返回的结果转换为布尔值。
// 添加文件记录
const addFile = async (file) => {
    const res = await File.create(file)
    return Boolean(res)
}

// 定义了一个名为 getFileList 的异步函数，用于获取文件列表。
// 它接受两个参数 userId 和 lastTime，分别表示要获取文件列表的用户 ID 和上次获取文件记录的时间。
// 在函数内部，使用 File.findAll() 方法查询数据库，条件是用户 ID 等于 userId，并且创建时间早于 lastTime。
// 同时，使用 limit 限制查询结果最多返回 30 条记录，并按照创建时间降序排列。
// 最后，通过 raw: true 选项指定返回原始对象形式的查询结果。
// 获取文件列表
const getFileList = async (userId, lastTime) => {
    return await File.findAll({
        where: {
            userId,
            createdAt: {
                [Op.lt]: lastTime,
            }
        },
        limit: 30,
        order: [
            ['createdAt', 'DESC'],
        ],
        raw: true,
    })
}

// 通过 module.exports 导出了 addFile 和 getFileList 两个函数，以便在其他文件中使用
module.exports = {
    addFile,
    getFileList,
}
