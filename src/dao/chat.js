// 引入了 Chat 模型和 Sequelize 中的运算符 Op
const Chat = require('../models/chat.js')
const {Op} = require("sequelize");

// 定义了一个名为 addChat 的异步函数，用于添加聊天记录。
// 它接受一个参数 chat，表示要添加的聊天记录。
// 在函数内部，使用 Chat.create(chat) 方法将 chat 对象保存到数据库中，并将结果赋值给变量 res。
// 然后，函数返回了一个布尔值，表示聊天记录是否成功添加，这里使用 Boolean(res) 将 Sequelize 返回的结果转换为布尔值。
// 添加聊天记录
const addChat = async (chat) => {
    const res = await Chat.create(chat)
    return Boolean(res)
}

// 定义了一个名为 getChatList 的异步函数，用于获取聊天列表。
// 它接受两个参数 sessionId 和 lastTime，分别表示会话 ID 和上次获取聊天记录的时间。
// 在函数内部，使用 Chat.findAll() 方法查询数据库，条件是会话 ID 等于 sessionId，并且更新时间小于 lastTime。
// 同时，使用 limit 限制查询结果最多返回 30 条记录，并按照更新时间降序排列。
// 最后，通过 attributes 属性指定要查询的字段，并将查询结果以原始对象的形式返回（raw: true）。
// 获取聊天列表
const getChatList = async (sessionId, lastTime) => {
    return await Chat.findAll({
        where: {
            sessionId,
            updatedAt: {
                [Op.lt]: lastTime
            }
        },
        limit: 30,
        order: [
            ['updatedAt', 'DESC'],
        ],
        attributes: ['senderId', 'receiverId', 'content', 'type', 'updatedAt', 'sort', 'others'],
        raw: true,
    })
}

// 通过 module.exports 导出了 addChat 和 getChatList 两个函数，以便在其他文件中使用。
module.exports = {
    addChat,
    getChatList,
}
