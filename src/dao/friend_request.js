// 引入了 FriendRequest 和 User 模型以及 Sequelize 中的运算符 Op
const FriendRequest = require('../models/friend_requests.js')
const User = require('../models/user.js')
const {Op} = require("sequelize");

// 定义了一个名为 getRequest 的异步函数，用于判断指定用户发送给指定好友的好友请求是否存在。
// 它接受两个参数 userId 和 friendId，分别表示用户 ID 和好友 ID。
// 在函数内部，使用 FriendRequest.findOne() 方法查询数据库，条件是发送者 ID 等于 userId 且接收者 ID 等于 friendId。
// 查询结果将作为原始对象返回。
// 判断好友请求是否存在
const getRequest = async (userId, friendId) => {
    const res = await FriendRequest.findOne({
        where: {
            // id: id
            userId: userId,
            friendId: friendId
        },
        raw: true,
    })
    return res
}

// 定义了一个名为 getRequestById 的异步函数，用于根据请求 ID 获取请求详情。
// 它接受一个参数 requestId，表示请求的唯一标识。
// 在函数内部，使用 FriendRequest.findOne() 方法查询数据库，条件是请求 ID 等于 requestId。
// 查询结果将作为原始对象返回。
// 根据id获取请求
const getRequestById = async (requestId) => {
    const res = await FriendRequest.findOne({
        where: {
            id: requestId
        },
        raw: true,
    })
    return res
}

// 定义了一个名为 addRequest 的异步函数，用于保存好友请求。
// 它接受三个参数 userId、friendId 和 content，分别表示发送请求的用户 ID、接收请求的好友 ID 和请求的内容。
// 在函数内部，获取当前时间戳作为请求的创建时间，然后使用 FriendRequest.create() 方法将请求信息保存到数据库中。
// 最后，函数返回一个布尔值，表示请求是否成功添加。
// 保存好友请求
const addRequest = async (userId, friendId, content) => {
    let timestamps = new Date().getTime()
    const res = await FriendRequest.create({
        userId,
        friendId,
        content,
        createdAt: timestamps,
    })
    return Boolean(res)
}

// 定义了一个名为 updateRequest 的异步函数，用于更新好友请求的状态和内容。
// 它接受五个参数 requestId、read、handle、content 和 createdAt，分别表示请求的唯一标识、是否已读、是否已处理、请求的内容和请求的创建时间。
// 在函数内部，使用 FriendRequest.update() 方法更新数据库中指定请求 ID 的记录，将传入的状态和内容更新到数据库中。
// 最后，函数返回一个布尔值，表示更新是否成功。
// 更新好友请求
const updateRequest = async (requestId, read = 0, handle = 0, content, createdAt) => {
    const res = await FriendRequest.update({handle, read, content, createdAt}, {
        where: {
            id: requestId
        },
        raw: true,
    })
    return Boolean(res)
}

// 定义了一个名为 getRequestList 的异步函数，用于获取用户收到的好友请求列表。
// 它接受两个参数 userId 和 lastTime，分别表示用户 ID 和上次获取请求列表的时间。
// 在函数内部，使用 FriendRequest.findAll() 方法查询数据库，条件是接收者 ID 等于 userId 且请求创建时间早于 lastTime。
// 同时，通过 include 选项关联了 User 模型，以获取请求发送者的信息。
// 最后，按照请求创建时间降序排列，并限制最多返回 20 条记录。
// 获取好友请求列表
const getRequestList = async (userId, lastTime) => {
    FriendRequest.belongsTo(User, {foreignKey: 'friendId', targetKey: 'id'});
    lastTime = lastTime || new Date().getTime()
    return await FriendRequest.findAll({
        where: {
            userId,
            createdAt: {
                [Op.lt]: lastTime
            }
        },
        include: [{
            model: User,
            attributes: ['id', 'userName', 'avatar', 'email'],
        }],
        order: [
            ['createdAt', 'DESC']
        ],
        // raw: true,
        limit: 20,
    })
}

// 通过 module.exports 导出了所有定义的函数，以便在其他文件中使用
module.exports = {
    getRequest,
    addRequest,
    getRequestList,
    getRequestById,
    updateRequest,
}
