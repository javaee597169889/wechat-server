// 引入了 Notification 模型和 Sequelize 中的 Op 对象
const Notification = require('../models/notifications.js')
const {Op} = require("sequelize");

// 定义了一个名为 addNotification 的异步函数，用于添加通知记录。
// 它接受三个参数 userId、type 和 createdAt，分别表示用户 ID、通知类型和创建时间。
// 在函数内部，使用 Notification.create() 方法向数据库中添加一条通知记录，并传入参数 userId、type 和 createdAt。
// 如果添加成功，则返回 true，否则返回 false。
// 添加通知记录
const addNotification = async (userId, type, createdAt) => {
    const res = await Notification.create({
        userId,
        type,
        createdAt,
    })
    return Boolean(res)
}

// 定义了一个名为 getNotificationList 的异步函数，用于获取指定用户的通知列表。
// 它接受两个参数 userId 和 lastTime，分别表示用户 ID 和上次获取通知的时间戳。
// 在函数内部，使用 Notification.findAll() 方法查询数据库，条件是用户 ID 等于 userId，并且通知的创建时间早于 lastTime。
// 通过 limit 选项限制查询结果数量为 30 条，并通过 order 选项按创建时间降序排列。
// 最后，函数返回查询到的通知列表。
// 获取通知列表
const getNotificationList = async (userId, lastTime) => {
    return await Notification.findAll({
        where: {
            userId,
            createdAt: {
                [Op.lt]: lastTime
            }
        },
        limit: 30,
        order: [
            ['createdAt', 'DESC'],
        ],
        raw: true,
    })
}

// 通过 module.exports 导出了所有定义的函数，以便在其他文件中使用
module.exports = {
    addNotification,
    getNotificationList,
}
