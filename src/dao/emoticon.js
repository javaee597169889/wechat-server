// 引入了 Emoticon 模型
const Emoticon = require('../models/emoticons.js')

// 定义了一个名为 addEmoticon 的异步函数，用于添加表情。
// 它接受一个参数 emoticon，表示要添加的表情信息。
// 在函数内部，使用 Emoticon.create(emoticon) 方法将 emoticon 对象保存到数据库中，并将结果赋值给变量 res。
// 然后，函数返回了一个布尔值，表示表情是否成功添加，这里使用 Boolean(res) 将 Sequelize 返回的结果转换为布尔值，
// 并调用 res.get({ plain: true }) 方法将 Sequelize 模型实例转换为普通对象，以便返回给调用者。
// 添加表情
const addEmoticon = async (emoticon) => {
    const res = await Emoticon.create(emoticon)
    return Boolean(res) && res.get({plain: true})
}

// 定义了一个名为 getEmoticonList 的异步函数，用于获取表情列表。
// 它接受一个参数 userId，表示要获取表情列表的用户 ID。
// 在函数内部，使用 Emoticon.findAll() 方法查询数据库，条件是用户 ID 等于 userId，并按照创建时间降序排列。
// 最后，通过 raw: true 选项指定返回原始对象形式的查询结果。
// 获取表情列表
const getEmoticonList = async (userId) => {
    return await Emoticon.findAll({
        where: {
            userId,
        },
        order: [
            ['createdAt', 'DESC'],
        ],
        raw: true,
    })
}

// 通过 module.exports 导出了 addEmoticon 和 getEmoticonList 两个函数，以便在其他文件中使用
module.exports = {
    addEmoticon,
    getEmoticonList,
}
